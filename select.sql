use labor_sql;
#Task4.1
SELECT DISTINCT maker 
FROM product AS pc_product
WHERE type in ('PC') AND
NOT EXISTS (SELECT maker FROM product
    WHERE type= 'Laptop' AND maker = pc_product.maker);
#Task4.4
SELECT DISTINCT maker 
FROM product AS pc_product
WHERE type in ('PC') and
exists(select maker from product
where type = 'Laptop' and maker = pc_product.maker);
#Task4.10
select distinct o.ship, o.battle, b.date
from outcomes o
left join battles b on b.name = o.battle
where o.result = 'damaged'
and exists(
select *
from outcomes o2
left join battles b2 on b2.name = o2.battle
where o2.ship=o.ship
and b2.date > b.date);
#Task4.11
select distinct maker
from product
where type = 'pc' and
not model = any (select model from pc);
#Task4.12
select distinct model, price
from laptop
where price > (select max(price) from pc);